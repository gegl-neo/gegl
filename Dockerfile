FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > gegl.log'

COPY gegl .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' gegl
RUN bash ./docker.sh

RUN rm --force --recursive gegl
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD gegl
